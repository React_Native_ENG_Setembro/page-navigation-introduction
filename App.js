import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import HomeScreen from "./src/pages/Home/Home";
import DetailsScreen from "./src/pages/Details/Details";
import HomeDetailsScreen from './src/pages/HomeDetails/HomeDetails';

const homeFlow = createStackNavigator({
  Home: {
    screen: HomeScreen
  },
  HomeDetails: {
    screen: HomeDetailsScreen
  }
})

export default createBottomTabNavigator({
  Home: {
    screen: homeFlow
  },
  Details: {
    screen: DetailsScreen
  }
});
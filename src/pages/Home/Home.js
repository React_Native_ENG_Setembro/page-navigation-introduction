import React from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';

export default class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Home'
  };

  state = {
    username: "",
    password: ""
  };

  login() {
    alert(`Username: ${this.state.username}
    Password: ${this.state.password} `)
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>Home Page</Text>
        <TextInput 
          onChangeText={(username) => {
            this.setState({username});
          }}
          value={this.state.username}
          placeholder={"Type the username"}
        />
        <TextInput 
          secureTextEntry={true}
          onChangeText={(password) => {
            this.setState({password});
          }}
          value={this.state.password}
          placeholder={"Type the password"}
        />
        <Button
          title={"Login"}
          onPress={() => {
            this.login();
          }}
          />
        <Button 
          onPress={() => {
            this.props.navigation.navigate("HomeDetails", {
              id: 86,
              otherParam: 'anything you want here',
            });
          }}
          title={"Go To Details"}
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
